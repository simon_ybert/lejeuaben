import { Button } from 'react-native-elements';
import React from 'react';
import { Text, Image, StyleSheet, View, TouchableWithoutFeedback} from "react-native";

class CardDisplay extends React.Component {
	constructor(props) {
		super(props);
		this.input;
		this.state = {
			isLoading : false,
		};
	}

	componentWillReceiveProps(newProps) {
		this.setState({cardUrl: newProps.cardUrl});
		this.setState({currentRule: newProps.currentRule});
	}

	render() {
		return (
			<View style={styles.main_container}>
				<TouchableWithoutFeedback onPress={this.props.onPress}>
					<Image style={styles.image} source={{uri:this.state.cardUrl}}/>
				</TouchableWithoutFeedback>
				<Text style={styles.text}>{this.state.currentRule}</Text>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main_container: {
		backgroundColor: '#FFF',
		flex: 5,
		alignItems: "center",
		paddingTop: 10,
		paddingBottom: 0,
	},
	image: {
		backgroundColor: '#FFF',
		flex: 8,
		aspectRatio: 0.6,
		resizeMode: "contain",
	},
	text: {
		textAlign: "center",
		flex: 1,
	}
});

export default CardDisplay
