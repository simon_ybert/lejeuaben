import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Button } from 'react-native-elements';
import GameTitle from './GameTitle';



class Home extends React.Component {
    render() {
        return (
            <View style={styles.main_container}>
               <GameTitle/>
                <View style={styles.img_container}>
                    <Image style={styles.img_ben} source={require('./img/ben.png')} />
                </View>
                <View style={styles.btn_container}>
                    <Button buttonStyle={{backgroundColor: 'green', width: 250}} title='Jouer'/>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#CFD8DC',
        flex: 1,
    },
    title_container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    img_container: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    img_ben: {
        height: 350,
        width: 250,
    },
    btn_play: {
        color: '#2E7D32',
        backgroundColor: '#fff',
        fontSize: 30
    },
    btn_container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    title: {
        color: '#FF6D00',
        fontWeight: 'bold',
        fontSize: 50
    }
});

export default Home
