import {Alert, Modal, ScrollView, StyleSheet, Text, Vibration, View} from "react-native";
import {Button, Icon, ListItem} from "react-native-elements";
import TouchableScale from "react-native-touchable-scale";
import React from "react";
import { sendtodrink, getLstPlayer } from "../API/LJABApi";


class drinkDistributor extends React.Component {

	constructor(props) {
		super(props);
		this.input;
		this.state = {
			gameid: '',
			player_drink: [],
			isLoading: false,
			player_list: []
		}
	}

	componentWillReceiveProps(newProps) {
		this.setState({gameid: newProps.gameid});
		this.setState({player_list: newProps.player_list});
		this.setState({modalVisible: newProps.modalVisible});
		if (this.state.modalVisible === false) {
			this.setState({drinkToDistribute: newProps.drinkToDistribute});
			this.initPlayerDrink();
		}
	}

	initPlayerDrink(){
		this.setState({player_drink: []});
		var tab=[]
		for(var i=0; i<this.state.player_list.length; i++){
			tab.push(0)
		}
		this.setState({player_drink: tab});
	}

	addDrink(player){
		if(this.state.drinkToDistribute>0){
			this.setState({drinkToDistribute: this.state.drinkToDistribute-1});
			var tab = this.state.player_drink;
			tab[player] += 1;
			this.setState({player_drink: tab});
		}
		else{
			Vibration.vibrate([1], false)
		}

	}

	deleteDrink(player){
		if(this.state.player_drink[player]>0){
			this.setState({drinkToDistribute: this.state.drinkToDistribute+1});
			var tab = this.state.player_drink;
			tab[player] -= 1;
			this.setState({player_drink: tab});
		}
		else {
			Vibration.vibrate([1], false)
		}

	}


	_sendtodrink() {
		sendtodrink(this.state.gameid, this.state.player_list, this.state.player_drink).then( ()=> {
				this.setState({modalVisible: false})
			}
		)
	}


	render() {
		return (
			<View style={{marginTop: 22}}>
				<Modal
					animationType="slide"
					transparent={false}
					visible={this.state.modalVisible}
					onRequestClose={() => {
						Alert.alert('Modal has been closed.');
					}}>
					<View style={{marginTop: 22}}>
						<View style={{marginTop: 50}}>
							<View style={styles.title_container}>
								<Text style={styles.title}>Distribue à boire !</Text>
							</View>
							<View style={styles.player_list_container}>
								<Text style={{marginBottom: 20}}>Gorgées restantes
									: {this.state.drinkToDistribute}</Text>
								<ScrollView>
									{
										this.state.player_list.map((l, i) => (
											<ListItem
												Component={TouchableScale}
												key={i}
												leftAvatar={{source: {uri: l.avatar_url}}}
												title={l}
												subtitle={l.subtitle}
												bottomDivider
												chevron={
													<View style={{flexDirection: 'row', alignItems: 'center'}}>
														<Text
															style={{marginRight: 20}}>{this.state.player_drink[i]}</Text>
														<View>
															<Icon
																name='keyboard-arrow-up'
																color='green'
																onPress={() => this.addDrink(i)}/>
															<Icon
																name='keyboard-arrow-down'
																color='red'
																onPress={() => this.deleteDrink(i)}/>
														</View>
													</View>
												}
											/>
										))
									}
								</ScrollView>
							</View>
							<View style={{alignContent: "flex-end", alignItems: 'center'}}>
								<Button
									title='A votre santé ! 🍺'
									buttonStyle={{backgroundColor: 'grey', width: 250, marginTop: 20}}
									onPress={() => {
										this._sendtodrink();
									}}/>
							</View>
						</View>
					</View>
				</Modal>
			</View>
		)
	}
}


const styles = StyleSheet.create({
	test: {
		backgroundColor: "red",
	},
	title_container: {
		alignItems: 'center',
	},
	title: {
		color: '#FF6D00',
		fontWeight: 'bold',
		fontSize: 30
	},
});

export default drinkDistributor
