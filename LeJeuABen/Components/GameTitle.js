import {StyleSheet, Text, View} from "react-native";
import React from "react";

class GameTitle extends React.Component {
    render() {
        return (
            <View style={styles.title_container}>
                <Text style={styles.title}>Le jeu à Ben</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    title_container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    title: {
        color: '#FF6D00',
        fontWeight: 'bold',
        fontSize: 50
    }
});

export default GameTitle