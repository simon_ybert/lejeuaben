import React from 'react';
import { Text, Image, StyleSheet, View} from "react-native";
import {Icon} from "react-native-elements";

class InfoDisplay extends React.Component {
	constructor(props) {
		super(props);
		this.input;
		this.state = {
			isLoading: false,
			color: "yellow",
		};
	}

	componentWillReceiveProps(newProps) {
		this.setState({title: newProps.title});
		this.setState({text: newProps.text});
		if (newProps.color !== undefined) {
			this.setState({color: newProps.color});
		}
	}

	render() {
		return (
			<View style={[styles.main_container, {backgroundColor : this.state.color,}]}>
				<Text style={styles.title}>{this.state.title}</Text>
				<Text style={styles.text}>{this.state.text}</Text>
			</View>

		)
	}
}

const styles = StyleSheet.create({
	main_container: {
		width: 70,
		height: 70,
		borderRadius: 50,
		margin: 5,
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
	},
	title: {
		fontWeight: "bold",
		textAlign: "center",
	},
	text: {
		paddingTop: 5,
		textAlign: "center",
	},
});

export default InfoDisplay
