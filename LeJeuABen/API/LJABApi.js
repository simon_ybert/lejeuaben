const API_URL = "http://seamless.servebeer.com:5000";

export function createParty() {
    const url = API_URL + '/mj/create';
    return fetch(url)
        .then((response) => response.json())
        .catch((error) => console.error(error))
}

export function addPlayer(gameid, name) {
    const url = API_URL + '/mj/addplayer/' + gameid;
    return fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            player: name,
        }),
    })
        .then((response) => response.json())
        .catch((error) => console.error(error))
}

export function deletePlayer(gameid, name) {
    const url = API_URL + '/mj/deleteplayer/' + gameid;
    return fetch(url, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            player: name,
        }),
    })
        .then((response) => response.json())
        .catch((error) => console.error(error))
}

export function getLstPlayer (gameid) {
    const url = API_URL + '/player/searchgame/' + gameid;
    return fetch(url)
        .then((response) => response.json())
        .catch((error) => console.error(error))
}

export function getGameStatus (gameid, player) {
    const url = API_URL + '/game/status/' + gameid + '/' + player;
    return fetch(url)
        .then((response) => response.json())
        .catch((error) => console.error(error))
}

export function tireUneCarte (gameid) {
    const url = API_URL + '/game/' + gameid + '/draw';
    return fetch(url)
        .then((response) => response.json())
        .catch((error) => console.error(error))
}

export function sendcleardrink (gameid, player) {
    const url = API_URL + '/game/' + gameid + '/cleardrinks/'+player;
    return fetch(url)
        .then((response) => response.json())
        .catch((error) => console.error(error))
}

export function sendtodrink(gameid, playertab, drinktab) {
    const url = API_URL + '/game/todrink/' + gameid;
    return fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            playertab: playertab,
            drinktab: drinktab,
        }),
    })
        .then((response) => response.json())
        .catch((error) => console.error(error))
}
