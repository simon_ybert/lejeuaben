import React from 'react';
import {StyleSheet, Text, View, Image, ScrollView, TextInputComponent} from 'react-native';
import {Button, Input, Icon, ListItem} from "react-native-elements";
import TextInput from "react-native-web/src/exports/TextInput";
import { createParty, addPlayer, getLstPlayer, deletePlayer } from "../API/LJABApi";
import TouchableScale from "react-native-touchable-scale";

class MJTitleScreen extends React.Component {
	static navigationOptions = {
		header: null,
	};

	constructor(props) {
		super(props);
		this.input;
		this.state = {
			textInput : '',
			gameid : '',
			isLoading : false,
			player_list : [
			]
		}
	}

	componentWillMount (){
		createParty().then(data => {
			this.setState({gameid: data["gameId"]});
		});
	}

	_addPlayer (){
		if (this.state.textInput.length > 0) {
			addPlayer(this.state.gameid, this.state.textInput).then(data => {
				this.setState({player_list: data["lstPlayers"]});
				this.setState({textInput: ''})
			});
		}
	}

	_deletePlayer (i) {
		deletePlayer(this.state.gameid, this.state.player_list[i]).then(data => {
			this.setState({player_list:data["lstPlayers"]})
		});
	}

	_PlayerInputChanged (name){
		this.setState({textInput:name});
	}


	render() {
		const {navigate} = this.props.navigation;
		return (
			<View style={[styles.main_container]}>
				<View style={[styles.title_container]}>
					<Text style={[styles.title]}>Le jeu à Ben</Text>
				</View>
				<View style={[styles.room_number_block]}>
					<View style={[styles.room_number_container]}>
						<Text style={[styles.room_number_text]}>Id de la partie : </Text>
						<Text style={[styles.room_number]}>{this.state.gameid}</Text>
					</View>
				</View>
				<View>
					<Text style={[styles.form_title_text]}>Participants</Text>
				</View>
				<View style={[styles.form_container]}>
					<View style={[styles.name_input_view]}>
						<Input style={[styles.name_input]} placeholder="Nom du joueur" clearButtonMode="always" onChangeText={(text) => this._PlayerInputChanged(text)} value={this.state.textInput} onSubmitEditing={() => this._addPlayer()}/>
					</View>
					<View style={[styles.add_button_view]}>
						<Button style={[styles.add_button]} buttonStyle={[styles.add_button_plus]} title='+' onPress={() => this._addPlayer()}/>
					</View>
				</View>
				<ScrollView style={[styles.scrollview]}>
					{
						this.state.player_list.map((l, i) => (
							<ListItem
								Component={TouchableScale}
								style={{width: 350}}
								key={i}
								leftAvatar={{ source: { uri: l.avatar_url } }}
								title={l}
								bottomDivider
								delete
								chevron={<Icon
									name='delete'
									color='red'
									onPress={() => this._deletePlayer(i)}/>}
							/>
						))
					}
				</ScrollView>
				<View style={[styles.btn_container]}>
					<Button buttonStyle={{backgroundColor: 'green', width: 250}} title='Jouer' onPress={() => navigate('MJGameScreen', {gameid:this.state.gameid, player:this.state.player_list[0]})}/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	test: {
		backgroundColor: "red",
	},
	main_container: {
		flex: 1,
		flexDirection: "column",
		justifyContent: "space-between",
		marginTop: 50
	},
	title_container: {
		height: 60,
		alignItems: 'center',
	},
	room_number_block: {
		flexDirection: "row-reverse",
		alignItems: "flex-start",
	},
	room_number_container: {
		margin: 5,
		textAlign: 'center',
		height: 100,
		width: 100,
		borderRadius: 75,
		backgroundColor: 'orange',
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "space-around",
	},
	room_number_text: {
		textAlign: "center",
		margin: 2,
		padding: 0,
		color: "white",
	},
	room_number: {
		fontWeight: "bold",
		fontSize: 20,
		margin: 2,
		marginTop: -30,
		padding: 0,
		color: "white",
	},
	form_title_text: {
		textAlign: "center",
		color: "orange",
		fontSize: 20,
		fontWeight: "bold",
	},
	form_container: {
		margin: 10,
		flexDirection: "row",
		alignItems: "center",
	},
	form_content_text: {
		textAlign: "center",
	},
	name_input_view: {
		flex: 1,
		alignItems: "flex-start",
		height: 40,
	},
	name_input: {
		height: 30,
		margin: 5,
		borderColor: "gray",
		borderWidth: 0.5,
		borderRadius: 1,
	},
	add_button_view: {
		marginTop: -40,
	},
	add_button: {
		flex: 1,
		borderRadius: 10,
	},
	add_button_plus: {
		backgroundColor: 'green',
		width: 40,
		height: 40
	},
	scrollview: {
		marginTop: 10,
		flex: 5,
	},
	img_container: {
		flex: 2,
		alignItems: 'center',
		justifyContent: 'center',
	},
	btn_play: {
		color: 'green',
		backgroundColor: '#fff',
		fontSize: 30
	},
	btn_container: {
		alignItems: 'center',
		backgroundColor: '#fff',
		justifyContent: 'flex-end',
		marginTop: 20,
		marginBottom: 70,
	},
	title: {
		color: 'orange',
		fontWeight: 'bold',
		fontSize: 50
	}
});

export default MJTitleScreen
