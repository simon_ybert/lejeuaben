import React from 'react';
import {StyleSheet, Text, View, Image, ScrollView, TextInputComponent} from 'react-native';
import {Button, Input, Icon, ListItem} from "react-native-elements";
import TextInput from "react-native-web/src/exports/TextInput";
import {createParty, addPlayer, getLstPlayer, deletePlayer, sendcleardrink} from "../API/LJABApi";
import TouchableScale from "react-native-touchable-scale";
import MJTitleScreen from "./MJTitleScreen";
import InfoDisplay from "../Components/InfoDisplay";
import GameTitle from "../Components/GameTitle";
import CardDisplay from "../Components/CardDisplay";
import { getGameStatus, tireUneCarte } from "../API/LJABApi";
import DrinkDistributor from "../Components/DrinkDistributor";

class MJGameScreen extends React.Component {
	static navigationOptions = {
		header: null,
	};

	constructor(props) {
		super(props);
		this.input;
		this.state = {
			modalVisible: false,
			gameid : props.navigation.state.params.gameid,
			player : props.navigation.state.params.player,
			currentCard : {},
			me : {},
			roiDesPouces : '',
			vipere : '',
			isLoading : false,
		}
	}

	_reloadGameState() {
		getGameStatus(this.state.gameid, this.state.player).then(data => {
			this.setState({currentPlayer: data["currentPlayer"]});
			this.setState({currentCard: data["currentCard"]});
			this.setState({currentRule: data["currentRule"]});
			this.setState({me: data["me"]});
			this.setState({roiDesPouces: data["roiDesPouces"]});
			this.setState({vipere: data["vipere"]});
			this.setState({cardRemaining: data["cardRemaining"]});
			if(data["me"]["giveDrink"] > 0 && this.state.modalVisible === false && this.state.last_distribute !== data["cardRemaining"]){
				this.setState({drinkToDistribute: data["me"]["giveDrink"]});
				this.setState({modalVisible: true});
				this.setState({last_distribute: data["cardRemaining"]});
				this.initPlayerDrink()
			} else if (data["me"]["giveDrink"] === 0 ) {
				this.setState({modalVisible: false});
			}
		});
	}

	initPlayerDrink(){
		this.setState({player_drink: []});
		var tab=[];
		for(var i=0; i<this.state.player_list.length; i++){
			tab.push(0)
		}
		this.setState({player_drink: tab});

	}

	_tireUneCarte() {
		tireUneCarte(this.state.gameid).then(e => {
			this._reloadGameState();
			this.initPlayerDrink();
		});
	}

	componentWillMount() {
		getLstPlayer(this.state.gameid).then(data => {
			this.setState({player_list: data.lstPlayers});
		})
	}

	componentDidMount() {
		this._interval = setInterval(() => {
			this._reloadGameState()
		}, 1000);
	}

	componentWillUnmount() {
		clearInterval(this._interval);
	}

	_sendcleardrink(){
		sendcleardrink(this.state.gameid, this.state.player)
		this._reloadGameState();
	}

	render() {
		return (
			<View style={styles.main_container}>
				<GameTitle style={styles.title}/>

				<View style={{position:"absolute", left: -2, bottom: 15, zIndex: 1,}}>
					<InfoDisplay title={"🃏"} text={this.state.cardRemaining} color={"purple"}/>
				</View>

				<View style={styles.infoContainer}>
					<InfoDisplay title={"🥴"} text={this.state.me.drinks} color={"red"}/>
					<InfoDisplay title={"🍺"} text={this.state.me.to_drink} color={"blue"}/>
					<InfoDisplay title={"🐍"} text={this.state.vipere} color={"yellow"}/>
					<InfoDisplay title={"👍"} text={this.state.roiDesPouces} color={"grey"}/>
					<InfoDisplay title={"🥳"} text={this.state.gameid} color={"green"}/>
				</View>


				<View style={[styles.buttonContainer]}>
					<Button
						title="J'ai tout bu 🤢"
						buttonStyle={{backgroundColor: 'red', width: 250, marginTop: 20}}
						onPress={() => {
							this._sendcleardrink();
						}}/>
				</View>

				<CardDisplay cardUrl={this.state.currentCard["img"]} currentRule={this.state.currentPlayer + ' : ' + this.state.currentRule} onPress={() => this._tireUneCarte()}/>

				<DrinkDistributor modalVisible={this.state.modalVisible} drinkToDistribute={this.state.drinkToDistribute} player_list={this.state.player_list} gameid={this.state.gameid}/>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main_container: {
		backgroundColor: '#FFF',
		flex: 1,
		alignItems: "center",
	},
	title: {
		marginBottom: 10,
	},
	infoContainer: {
		flex: 1.5,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-around",
	},
	buttonContainer: {
		marginTop: 10,
	},
});

export default MJGameScreen
