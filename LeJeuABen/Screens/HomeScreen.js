import { Button } from 'react-native-elements';
import React from 'react';
import GameTitle from "../Components/GameTitle";
import {Image, StyleSheet, View} from "react-native";

class HomeScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };
    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.main_container}>
                <GameTitle/>
                <View style={styles.img_container}>
                    <Image style={styles.img_ben} source={require('../Components/img/ben.png')} />
                </View>
                <View style={styles.btn_container}>
                    <Button buttonStyle={{backgroundColor: '#FF6D00', width: 170}} onPress={() => navigate('MJTitleScreen')} title='Créer'/>
                    <Button buttonStyle={{backgroundColor: 'green', width: 170}} onPress={() => navigate('PlayerTitle')} title='Rejoindre'/>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    main_container: {
        backgroundColor: '#FFF',
        flex: 1,
    },
    title_container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    img_container: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    img_ben: {
        height: 350,
        width: 250,
    },
    btn_play: {
        color: '#2E7D32',
        backgroundColor: '#fff',
        fontSize: 30
    },
    btn_container: {
        flex: 1,
        marginTop: 50,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-around',
    },
    title: {
        color: '#FF6D00',
        fontWeight: 'bold',
        fontSize: 50
    }
});

export default HomeScreen
