import {Button, Icon, Input, ListItem} from 'react-native-elements';
import React from 'react';
import GameTitle from "../Components/GameTitle";
import {Vibration, Alert, Modal, TouchableHighlight, Image, ScrollView, StyleSheet, Text, View} from "react-native";
import {getGameStatus, getLstPlayer, sendtodrink, sendcleardrink} from "../API/LJABApi";
import CardDisplay from "../Components/CardDisplay";
import TouchableScale from "react-native-touchable-scale";
import DrinkDistributor from "../Components/DrinkDistributor";

class PlayerGameScreen extends React.Component {
    constructor(props) {
        super(props);
        this.gameid = props.navigation.state.params.gameid;
        this.name = props.navigation.state.params.name;
        //this.gameid = 12345;
        //this.name = 'Tamoto';
        this.state = {
            modalVisible: false,
            currentCard : {},
            me : {},
            roiDesPouces : '',
            vipere : '',
            isLoading : false,
            player_list: [],
            drinkToDistribute: 0,
            player_drink: [],
            last_distribute: 0
        }
    }
    _getGameStatus() {
        getGameStatus(this.gameid, this.name).then(data => {
            this.setState({currentPlayer: data["currentPlayer"]});
            this.setState({currentCard: data["currentCard"]});
            this.setState({currentRule: data["currentRule"]});
            this.setState({me: data["me"]});
            this.setState({roiDesPouces: data["roiDesPouces"]});
            this.setState({vipere: data["vipere"]});
            this.setState({cardRemaining: data["cardRemaining"]});
            if(data["me"]["giveDrink"] > 0 && this.state.modalVisible === false && this.state.last_distribute !== data["cardRemaining"]){
                this.setState({drinkToDistribute: data["me"]["giveDrink"]});
                this.setState({modalVisible: true});
                this.setState({last_distribute: data["cardRemaining"]});
                this.initPlayerDrink()
            }
        });
    }

    componentDidMount() {
        this._interval = setInterval(() => {
            this._getGameStatus()
        }, 1000);
    }

    initPlayerDrink(){
        this.setState({player_drink: []});
        var tab=[]
        for(var i=0; i<this.state.player_list.length; i++){
            tab.push(0)
        }
        this.setState({player_drink: tab});

    }

    componentWillMount() {
        this._getGameStatus();
        getLstPlayer(this.gameid).then(data => {
            this.setState({player_list: data.lstPlayers});
            this.initPlayerDrink()
        })
    }

    componentWillUnmount() {
        clearInterval(this._interval);
    }

    _sendcleardrink(){
        sendcleardrink(this.gameid, this.name)
    }


    static navigationOptions = {
        header: null,
    };
    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={{flex: 1, backgroundColor: '#FFF'}}>
                <GameTitle/>
                <View style={style_player_game.main_container}>
                    <View style={{flexDirection: 'row', alignSelf: 'flex-start'}}>
                        <Text>🥴 : {this.state.me.drinks}</Text>
                        <Text>🍺 : {this.state.me.to_drink}</Text>
                        <Text>🐍 : {this.state.vipere}</Text>
                        <Text>👍 : {this.state.roiDesPouces}</Text>
                        <Text>🃏 : {this.state.cardRemaining}</Text>
                    </View>
                    <View style={{alignItems: 'center'}}>
                        <Button
                            title="J'ai tout bu 🤢"
                            buttonStyle={{backgroundColor: 'red', width: 250, marginTop: 20}}
                            onPress={() => {
                                this._sendcleardrink();
                            }}/>
                    </View>
                    <CardDisplay cardUrl={this.state.currentCard["img"]} currentRule={this.state.currentPlayer+' : '+this.state.currentRule}/>

                    <DrinkDistributor modalVisible={this.state.modalVisible} drinkToDistribute={this.state.drinkToDistribute} player_list={this.state.player_list} gameid={this.gameid}/>

                </View>
            </View>
        );
    }
}

const style_player_game = StyleSheet.create({
    main_container: {
        backgroundColor: '#FFF',
        flex: 7,
    },
    title_container: {
        alignItems: 'center',
    },
    img_container: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    img_card: {
        flex: 1,
        aspectRatio: 0.5,
        resizeMode: 'contain'
    },
    btn_play: {
        color: '#2E7D32',
        backgroundColor: '#fff',
        fontSize: 30
    },
    btn_container: {
        flex: 1,
        marginTop: 50,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-around',
    },
    title: {
        color: '#FF6D00',
        fontWeight: 'bold',
        fontSize: 30
    }
});

export default PlayerGameScreen
