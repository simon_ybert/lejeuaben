import { Button } from 'react-native-elements';
import { Input } from 'react-native-elements';
import { ListItem } from 'react-native-elements';
import { Keyboard } from 'react-native'
import TouchableScale from 'react-native-touchable-scale';
import {Alert, ScrollView, StyleSheet, Text, View} from "react-native";
import React from 'react';
import GameTitle from "../Components/GameTitle";
import { getLstPlayer } from "../API/LJABApi";

class PlayerTitleScreen extends React.Component {
    constructor(props) {
        super(props);
        this.gameid = '';
        this.state = {
            isLoading: false,
            player_list : [
            ]
        }
    }

    _searchTextInputChanged(text) {
        this.gameid = text; // Modification du texte recherché à chaque saisie de texte, sans passer par le setState comme avant
    }

    _searchPlayers() {
        Keyboard.dismiss();
        this.setState({
            player_lst : []
        }, () => {
            this._loadPlayers()
        })
    }

    _loadPlayers() {
        if (this.gameid.length > 0) {
            this.setState({ isLoading: true });
            getLstPlayer(this.gameid).then(data => {
                if(data.status === 200){
                    this.setState({player_list: data.lstPlayers});
                }
                else {
                    this.setState({player_list: []});
                    Alert.alert('Aucune partie ne correspond à cet ID.');
                }
            });
        }
    }

    static navigationOptions = {
        header: null,
    };

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={{flex: 1, backgroundColor: '#FFF'}}>
                <GameTitle/>
                <View style={style_player_title.main_container}>
                    <View style={style_player_title.search_container}>
                        <Input
                            keyboardType='numeric'
                            onChangeText={(text) => this._searchTextInputChanged(text)}
                            onSubmitEditing={() => this._searchPlayers()}
                            placeholder='ID de la partie'
                        />
                        <Button
                            buttonStyle={{backgroundColor: '#FF6D00', width: 250, marginTop: 20}}
                            onPress={() => this._searchPlayers()}
                            title='Rechercher'
                        />
                    </View>
                    <View style={style_player_title.player_list_container}>
                        <Text style={{marginBottom: 20}}>Sélectionnez votre profil</Text>
                        <ScrollView>
                            {
                                this.state.player_list.map((l, i) => (
                                    <ListItem
                                        Component={TouchableScale}
                                        style={{width: 350}}
                                        key={i}
                                        leftAvatar={{ source: { uri: l.avatar_url } }}
                                        title={l}
                                        subtitle={l.subtitle}
                                        onPress={() => navigate('PlayerGame', {name: l, gameid: this.gameid})}
                                        bottomDivider
                                        chevron
                                    />
                                ))
                            }
                        </ScrollView>
                    </View>
                </View>
            </View>
        );
    }
}

const style_player_title = StyleSheet.create({
    main_container: {
        flex: 7
    },
    search_container: {
        flex: 1,
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center',
    },
    player_list_container: {
        flex: 3,
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'flex-start',
    }
});
export default PlayerTitleScreen
