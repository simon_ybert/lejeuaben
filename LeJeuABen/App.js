import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import HomeScreen from './Screens/HomeScreen';
import PlayerTitleScreen from './Screens/PlayerTitleScreen';
import MJTitleScreen from "./Screens/MJTitleScreen";
import MJGameScreen from "./Screens/MJGameScreen";
import PlayerGameScreen from "./Screens/PlayerGameScreen";
import React from 'react';

const MainNavigator = createStackNavigator({
    Home: {screen: HomeScreen},
    PlayerTitle: {screen: PlayerTitleScreen},
    MJTitleScreen: {screen: MJTitleScreen},
    PlayerGame: {screen: PlayerGameScreen},
    MJGameScreen: {screen: MJGameScreen},
});

const App = createAppContainer(MainNavigator);

export default App;
