from flask import Flask, request, jsonify
import json
from random import randint
import requests
CARD_API_URL = 'https://deckofcardsapi.com/api/deck'
app = Flask(__name__)
app.config["DEBUG"] = True


class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


new_player_model = {
    'place': -1,
    'drinks': 0,
    'isRoiDesPouces': False,
    'isVipere': False
}

new_card_model = {
    'value': '',
    'img': ''
}


lstRules = {
    'ACE': 'Distribue 1 gorgée',
    '2': 'Distribue 2 gorgées',
    '3': 'Distribue 3 gorgées',
    '4': 'Distribue 4 gorgées',
    '5': 'Distribue 5 gorgées',
    '6': "Je n'ai jamais",
    '7': "J'ai déjà",
    '8': 'Tout le monde bois',
    '9': 'Nouvelle règle',
    '10': 'Vipère',
    'JACK': 'Roi des pouces',
    'QUEEN': 'Les dames boivent',
    'KING': 'Les rois boivent',
    'JOKER': 'Cul-sec',
}

lstGames = {
    12345: {
        'players':
            {
                'Tamoto':
                    {
                        'place': 1,
                        'drinks': 0,
                        'to_drink': 0,
                        'giveDrink': 0,
                        'isRoiDesPouces': True,
                        'isVipere': False
                    },
                'Anael':
                    {
                        'place': 2,
                        'drinks': 0,
                        'to_drink': 0,
                        'giveDrink': 0,
                        'isRoiDesPouces': False,
                        'isVipere': False
                    }
            },
        "currentCard": {
            "img": "https://deckofcardsapi.com/static/img/2D.png",
            "value": "2"
        },
        'deckId': 'kx0vkt3o34uh',
        'row': 1,
        'cardRemaining': 52
    }
}


def updatedrinks(gameid, player, todrink):
    lstGames[gameid]['players'][player]['drinks'] += todrink


def getCurrentRule(gameid):
    return lstRules[lstGames[gameid]['currentCard']['value']]


def getroidespouces(gameid):
    for i in list(lstGames[gameid]['players']):
        if lstGames[gameid]['players'][i]['isRoiDesPouces']:
            return i
    return None


def getvipere(gameid):
    for i in list(lstGames[gameid]['players']):
        if lstGames[gameid]['players'][i]['isVipere']:
            return i
    return None


def getmaxplace(gameid):
    maxplace = 0
    for i in list(lstGames[gameid]['players']):
        if lstGames[gameid]['players'][i]['place']>maxplace:
            maxplace = lstGames[gameid]['players'][i]['place']
    return maxplace



def updatecard(card):
    new_card = {
        'value': card['value'],
        'img': card['image']
    }
    return new_card


def prettyprint(dic):
    print(json.dumps(dic, indent=4, sort_keys=True))


def getplayerlist(gameid):
    playertab = []
    for i in list(lstGames[int(gameid)]['players']):
        playertab.append(i)
    return playertab


def cardleft(gameid):
    return len(lstGames[gameid]['cardsLeft'])


def createnewapideck():
    return requests.get(CARD_API_URL+'/new/draw/?deck_count=1').json()


def setvipere(gameid):
    if getvipere(gameid) is not None:
        lstGames[gameid]['players'][getvipere(gameid)]['isVipere'] = False
    lstGames[gameid]['players'][getCurrentPlayer(gameid)]['isVipere'] = True


def setroidespouces(gameid):
    if getroidespouces(gameid) is not None:
        lstGames[gameid]['players'][getroidespouces(gameid)]['isRoiDesPouces'] = False
    lstGames[gameid]['players'][getCurrentPlayer(gameid)]['isRoiDesPouces'] = True


def drawapicard(gameid):
    r = requests.get(CARD_API_URL+'/'+lstGames[gameid]['deckId']+'/draw/?count=1').json()
    lstGames[gameid]['cardRemaining'] = r['remaining']
    lstGames[gameid]['currentCard'] = {
        'value': r['cards'][0]['value'],
        'img': r['cards'][0]['image']
    }
    if lstGames[gameid]['currentCard']['value'] == '10':
        setvipere(gameid)

    if lstGames[gameid]['currentCard']['value'] == 'JACK':
        setroidespouces(gameid)

    if lstGames[gameid]['currentCard']['value'] == 'ACE':
        lstGames[gameid]['players'][getCurrentPlayer(gameid)]['giveDrink'] = 1

    if lstGames[gameid]['currentCard']['value'] == '2':
        lstGames[gameid]['players'][getCurrentPlayer(gameid)]['giveDrink'] = 2

    if lstGames[gameid]['currentCard']['value'] == '3':
        lstGames[gameid]['players'][getCurrentPlayer(gameid)]['giveDrink'] = 3

    if lstGames[gameid]['currentCard']['value'] == '4':
        lstGames[gameid]['players'][getCurrentPlayer(gameid)]['giveDrink'] = 4

    if lstGames[gameid]['currentCard']['value'] == '5':
        lstGames[gameid]['players'][getCurrentPlayer(gameid)]['giveDrink'] = 5


def getCurrentPlayer(gameid):
    game = lstGames[gameid]
    players = getplayerlist(gameid)
    return players[game['row']%len(players)]


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route('/admin/games')
def getAllGames():
    return jsonify(lstGames)


@app.route('/mj/create', methods=['GET'])
def creategame():
    new_id = ''.join(["{}".format(randint(0, 9)) for num in range(0, 5)])
    new_deck = createnewapideck()
    lstGames[int(new_id)] = {
        'players': {},
        'currentCard': {},
        'deckId': '',
        'row': 0,
        'cardRemaining': 52
    }
    lstGames[int(new_id)]['currentCard'] = updatecard(new_deck['cards'][0])
    lstGames[int(new_id)]['deckId'] = new_deck['deck_id']
    prettyprint(lstGames)
    return jsonify(
        {
            'status': 200,
            'gameId': new_id
        }
    )


@app.route('/mj/deleteplayer/<gameid>', methods=['DELETE'])
def deleteplayer(gameid):
    gameid = int(gameid)
    player = request.get_json()
    player = player['player']
    del lstGames[gameid]['players'][player]
    return jsonify(
        {
            'status': 200,
            'lstPlayers': getplayerlist(gameid)
        }
    )


@app.route('/mj/addplayer/<gameid>', methods=['POST'])
def addplayer(gameid):
    try:
        gameid = int(gameid)
    except:
        raise InvalidUsage('Game not found', status_code=404)
    else:
        new_player = request.get_json()
        new_player = new_player['player']
        if new_player in getplayerlist(gameid):
            return jsonify({
                'status': 200,
                'lstPlayers': getplayerlist(gameid),
            })
        lstGames[gameid]['players'][new_player] = {
            "drinks": 0,
            'to_drink': 0,
            "isRoiDesPouces": False,
            "isVipere": False,
            "place": 0
            }
        lstGames[gameid]['players'][new_player]['place'] = getmaxplace(gameid)+1
        print(lstGames[int(gameid)])
        return jsonify(
            {
                'lstPlayers': getplayerlist(gameid),
                'status': 200
            }
        )


@app.route('/game/<gameid>/draw', methods=['GET'])
def drawcard(gameid):
    gameid = int(gameid)
    lstGames[gameid]['row'] += 1
    drawapicard(int(gameid))
    print(getCurrentPlayer(gameid))
    return jsonify({
        'status': 200,
        'player': getCurrentPlayer(gameid)
    })

@app.route('/game/todrink/<int:gameid>', methods=['POST'])
def todrink(gameid):
    result = request.get_json()
    player_tab = result['playertab']
    drink_tab = result['drinktab']
    for i in range(len(player_tab)):
        lstGames[gameid]['players'][player_tab[i]]['to_drink'] += drink_tab[i]
        lstGames[gameid]['players'][player_tab[i]]['giveDrink'] = 0
    return jsonify([])


@app.route('/game/<int:gameid>/cleardrinks/<player>', methods=['GET'])
def cleardrink(gameid, player):
    lstGames[gameid]['players'][player]['drinks'] += lstGames[gameid]['players'][player]['to_drink']
    lstGames[gameid]['players'][player]['to_drink'] = 0
    return jsonify([])


@app.route('/game/status/<int:gameid>/<player>', methods=['GET'])
def getgamestatus(gameid, player):
    game = lstGames[gameid]
    return jsonify({
        'status': 200,
        'me': game['players'][player],
        'currentCard': game['currentCard'],
        'roiDesPouces': getroidespouces(gameid),
        'vipere': getvipere(gameid),
        'currentPlayer': getCurrentPlayer(gameid),
        'currentRule': getCurrentRule(gameid),
        'cardRemaining': game['cardRemaining']
    })


@app.route('/player/searchgame/<gameid>', methods=['GET'])
def playersearchgame(gameid):
    try:
        gameid = int(gameid)

    except:
        return jsonify(
            {
                'status': 404
            }
        )
    else:
        for i in list(lstGames):
            if i==gameid:
                return jsonify(
                    {
                        'lstPlayers': getplayerlist(gameid),
                        'status': 200
                    }
                )
        return jsonify(
            {
                'status': 404
            }
        )


if __name__ == '__main__':
    app.run(host='0.0.0.0')
