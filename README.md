# Le Jeu à Ben

## Introduction

Ce projet est une version numérique du fameux jeu de carte 'Le Jeu à Ben'.
Une API permet de gérer le déroulement d'une partie et à chaque joueur puisse suivre en temps réel cette dernière.

## Installation

L'applications compilée (APK uniquement pour le moment) est disponible [ici](https://drive.google.com/open?id=1qKpR7Uc0l16mDJgQ5crKjTmzbFBLELDf).
Nous gardons une trace de toute les versions sur le drive.

#### API (Docker)

`cd API_LeJeuABen`\
`docker build -t ljabapi .`\
`docker run -p 5000:5000 -d ljabapi:latest`

## Built With

* [React Native](https://symfony.com/) - Framework Mobile
* [Flask](https://httpd.apache.org/) - Framework API

## Authors

* **Anaël Bourgneuf-Monclair** - *Etudiant Ecole404*
* **Simon YBERT** - *Etudiant Ecole404*


